"""
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import urllib2, requests
import pickle

"""
Este programa faz uma copia dos dados de artigos do site qualis.capes.gov.br.
Como: Atraves de requisicoes das paginas html do site, fornecidas para qualquer
pessoa que deseje acessa-las.
"""

def parseContent( htmlData, area, dictTitulo, dictEstrato ):
    """
    Processa uma pagina html em busca de itens da tabela de avaliacao de artigos.
    Args:
        htmlData: Pagina html como entrada, contendo N entradas de artigos.
        area: Inteiro indicando a area de avaliacao.
        dictTitulo: Dicionario para armazenar os titulos dos trabalhos.
        dictEstrato: Dicionario para armazenar os estratos.
    Return:
        1 se existem mais paginas a serem lidas apos esta.
        0 caso contrario
    """
    clasLocal = 0
    issnLocal = htmlData[clasLocal:].find("j_id199\"")
    while issnLocal-clasLocal != -1:
        
        issnEnd = issnLocal + htmlData[issnLocal:].find("<")
        issn = htmlData[issnLocal+55:issnEnd]
        
        titleLocal = issnLocal + htmlData[issnLocal:].find("j_id202\"")
        titleEnd = titleLocal + htmlData[titleLocal:].find("<")
        title = htmlData[titleLocal+9:titleEnd]
        
        estratoLocal = titleLocal + htmlData[titleLocal:].find("j_id205\"")
        estratoEnd = estratoLocal + htmlData[estratoLocal:].find("<")
        estrato = htmlData[estratoLocal+47:estratoEnd]
        if estrato[0] == 'C':
            estrato = 'C'
        
        clasLocal = estratoLocal + htmlData[estratoLocal:].find("j_id211\"")
        clasEnd = clasLocal + htmlData[clasLocal:].find("<")
        clas = htmlData[clasLocal+36:clasEnd]
        
        #print issn, title, area, clas
        
        if len(issn) > 0:
            dictTitulo[issn] = title
            dictEstrato[(issn, area)] = estrato
        else:
            dictTitulo[title] = title
            dictEstrato[(title, area)] = estrato
        issnLocal = clasLocal + htmlData[clasLocal:].find("j_id199\"")
        
    if htmlData.find("'page': 'next'" ) != -1:
        return 1

    return 0

"""
Sao necessarias tres requisicoes iniciais para que se chegue a pagina
que exibe a avaliacao dos artigos.
"""
urlBase = "http://qualis.capes.gov.br/webqualis/"
acessoInicial = requests.get(urlBase)
jid = acessoInicial.cookies['JSESSIONID']
#print jid
url1 = urlBase + "publico/pesquisaPublicaClassificacao.seam;jsessionid=" + jid + "?conversationPropagation=begin"
req1 = urllib2.Request(url1)
arq1 = urllib2.urlopen(req1)
arq1 = urllib2.urlopen(req1)

url2 = urlBase + "publico/pesquisaPublicaClassificacao.seam;jsessionid=" + jid
url2
req2 = urllib2.Request(url2, 'AJAXREQUEST=_viewRoot&consultaPublicaClassificacaoForm=consultaPublicaClassificacaoForm&consultaPublicaClassificacaoForm%3Aissn=&javax.faces.ViewState=j_id2&consultaPublicaClassificacaoForm%3Aj_id190=consultaPublicaClassificacaoForm%3Aj_id190&')
arq2 = urllib2.urlopen(req2)

req3 = urllib2.Request(url2, 'consultaPublicaClassificacaoForm=consultaPublicaClassificacaoForm&consultaPublicaClassificacaoForm%3AsomAreaAvaliacao=0&consultaPublicaClassificacaoForm%3AsomEstrato=org.jboss.seam.ui.NoSelectionConverter.noSelectionValue&consultaPublicaClassificacaoForm%3AbtnPesquisarTituloPorArea=Pesquisar&javax.faces.ViewState=j_id2')
arq3 = urllib2.urlopen (req3)

"""
A partir deste ponto, sao feitas requisicoes apenas do conteudo da
tabela de avaliacao, sendo exibidos 10 itens por vez de uma
determinada area.
"""

dictTitulo = {}
dictEstrato = {}

for area in range(0,49): # 49 areas de avaliacao ####
    scroller = 1
    more = 1
    reqn = urllib2.Request(url2, 'consultaPublicaClassificacaoForm=consultaPublicaClassificacaoForm&consultaPublicaClassificacaoForm%3AsomAreaAvaliacao=' + str(area) + '&consultaPublicaClassificacaoForm%3AsomEstrato=org.jboss.seam.ui.NoSelectionConverter.noSelectionValue&consultaPublicaClassificacaoForm%3AbtnPesquisarTituloPorArea=Pesquisar&javax.faces.ViewState=j_id2')
    arqn = urllib2.urlopen (reqn)
    while more == 1:
        print "Reading page:", scroller, "for area", area
        
        reqn = urllib2.Request(url2, 'AJAXREQUEST=_viewRoot&consultaPublicaClassificacaoForm=consultaPublicaClassificacaoForm&consultaPublicaClassificacaoForm%3AsomAreaAvaliacao=' + str(area) + '&consultaPublicaClassificacaoForm%3AsomEstrato=org.jboss.seam.ui.NoSelectionConverter.noSelectionValue&javax.faces.ViewState=j_id3&ajaxSingle=consultaPublicaClassificacaoForm%3AscrollerArea&consultaPublicaClassificacaoForm%3AscrollerArea=' + str(scroller) + '&AJAX%3AEVENTS_COUNT=1&')
        
        #arqn = urllib2.urlopen (reqn)

        ntries = 10
        for i in range(0,ntries):
            try:
                arqn = urllib2.urlopen(reqn)
                break # success
            except urllib2.URLError as err:
                print "Error occurried. Trying again."
                continue
                #if not isinstance(err.reason, socket.timeout):
                #    raise "Non timeout error occurried while loading page." # propagate non-timeout errors
                #else: # all ntries failed 
                #    raise err # re-raise the last timeout error
            if i == 10:
                print "ja tentou 10 vezes!"
                break

        htmln = arqn.read()
        more = parseContent( htmln, area, dictTitulo, dictEstrato )
        #f = open( str(area) + "." + str(scroller) + ".html", 'w')
        #f.write(htmln)
        #f.close()
        scroller += 1


fileTitulo = open('titulo.qp', 'w')
fileEstrato = open('area_estrato.qp', 'w')
pickle.dump(dictTitulo, fileTitulo)
pickle.dump(dictEstrato, fileEstrato)
fileTitulo.close()
fileEstrato.close()



