"""
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import pickle

class QualisReader:  
    def __init__(self, arqTitulos, arqEstratos):
        self.lerArquivoTitulos(arqTitulos)
        self.lerArquivoEstratos(arqEstratos)
    
    def lerArquivoTitulos(self, arquivo):
        try:
            f = open( arquivo, 'r')
        except IOError:
            print 'Erro: Arquivo nao pode ser aberto:', arquivo
        else:
            self.titulos = pickle.load(f)
            f.close()
    
    def lerArquivoEstratos(self, arquivo):
        try:
            f = open(arquivo, 'r')
        except IOError:
            print 'Erro: Arquivo nao pode ser aberto:', arquivo
        else:
            self.estratos = pickle.load(f)
            f.close()
    
    def getEstrato(self, issn, area):
        try:
            estrato = self.estratos[(issn,area)]
        except LookupError:
            print "Erro: A tupla (" + issn + ", " + str(area) + ") nao faz parte do banco."
        else:
            return estrato
            
    def getTitulo(self, issn):
        try:
            titulo = self.titulos[issn]
        except LookupError:
            print "Erro: O issn", issn, "nao faz parte do banco."
        else:
            return titulo
    
    def getTodosEstratos(self, issn):
        #estratos = sorted([(area, estrato) for ((issn2, area), estrato) in self.estratos.items() if issn2 == issn])
        #print estratosz
        estratos = {}
        for area in range(0, 48):
            try:
                estratos[area] = self.estratos[(issn,area)]
            except LookupError:
                estratos[area] = 0
                continue
        return estratos
        
    def getMelhorEstrato(self, issn):
        estratos = self.getTodosEstratos(issn)
        melhorEstrato = estratos[0]
        for area in range(1, 48):
            #print melhorEstrato, estratos[area]
            if self.__maior__(melhorEstrato, estratos[area]) == 1:
                melhorEstrato = estratos[area]
        return melhorEstrato
    
    def getMelhorArea(self, issn ):
        estratos = self.getTodosEstratos(issn)
        melhorEstrato = estratos[0], 0
        for area in range(1, 48):
            if self.__maior__(melhorEstrato[0], estratos[area]) == 1:
                melhorEstrato = estratos[area], area
        return melhorEstrato[1]
    
    def getMelhoresAreas(self, issn ):
        estratos = self.getTodosEstratos(issn)
        melhorEstrato = self.getMelhorEstrato(issn)
        melhoresEstratos = {}
        i = 0
        for area in range(1, 48):
            if self.__maior__(melhorEstrato, estratos[area]) == 0:
                melhoresEstratos[i] = area
                i += 1
        return melhoresEstratos
    
    
    def getTodosIssn(self):
		lista = []
		for key in self.estratos.keys():
			lista.append(key[0])
		return lista
		
    def getTodosNomes(self):
		listaIssn = self.getTodosIssn()
		listaNomes = []
		for issn in listaIssn:
			listaNomes.append(self.getTitulo(issn))
		return listaNomes
    
    def __maior__(self, estrato1, estrato2):
        """
        Compara dois estratos.
        Retorna -1 se estrato1 > estrato2
        Retorna  0 se estrato1 == estrato2
        Retorna  1 se estrato1 < estrato2
        """
        
        if estrato1 == 0:
            return 1
        if estrato2 == 0:
            return -1
        if estrato1 == estrato2:
            return 0
        if estrato1 == 'C':
            return 1
        if estrato2 == 'C':
            return -1        
        if estrato1[0] == estrato2[0]:
            if int(estrato1[1]) < int(estrato2[1]):
                return -1
            if int(estrato1[1]) == int(estrato2[1]):
                return 0
            if int(estrato1[1]) > int(estrato2[1]):
                return 1
        if estrato1[0] == 'A':
            return -1
        if estrato2[0] == 'A':
            return 1
        if estrato1[0] == 'B':
            return -1
        else:
            return 1


